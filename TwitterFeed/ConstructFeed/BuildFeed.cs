﻿using System;
using System.Collections.Generic;
using System.Linq;
using TwitterFeed.Configuration;
using TwitterFeed.Configuration.Contracts;
using TwitterFeed.Models;
using TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo.Contracts;
using TwitterFeed.Repositories.ServiceRepo.ReadFileRepo.Contracts;

namespace TwitterFeed.ConstructFeed
{
    public class BuildFeed
    {
        private IReadFileRepo _readFileRepo;
        private IPrintFeed _printFeed;
        private IConfigService _configService;


        public BuildFeed(IReadFileRepo readFileRepo, IPrintFeed printFeed, IConfigService configService)
        {
            _readFileRepo = readFileRepo ?? throw new ArgumentNullException(nameof(readFileRepo));
            _printFeed = printFeed ?? throw new ArgumentNullException(nameof(printFeed));
            _configService = configService ?? throw new ArgumentNullException(nameof(configService));
        }

        public void Build()
        {
            var userFileData = _readFileRepo.ReadUserFile(_configService.GetFullFilePath(Config.pathName, Config.UserFile));
            var tweetFileData = _readFileRepo.ReadTweetFile(_configService.GetFullFilePath(Config.pathName, Config.TweetFile));

            Console.WriteLine(_printFeed.PrintTweets(GetUserFollowers(userFileData), GetTweetMessages(tweetFileData)));
            Console.ReadLine();
        }

        private List<Follower> GetUserFollowers(Dictionary<string, List<string>> readUserFile)
        {
            if (readUserFile is null) throw new ArgumentNullException(nameof(readUserFile));

            return new List<Follower>(readUserFile.Select(x => new Follower { Name = x.Key, Follows = x.Value }));
        }

        private List<Message> GetTweetMessages(Dictionary<string, List<string>> tweets)
        {
            if (tweets == null) throw new ArgumentNullException(nameof(tweets));

            return new List<Message> ( tweets.Select(x => new Message { Name = x.Key, Tweet = x.Value }) );
        }
    }
}
