﻿using System;
using System.Threading.Tasks;
using TwitterFeed.Configuration;
using TwitterFeed.Configuration.Contracts;
using TwitterFeed.ConstructFeed;
using TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo;
using TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo.Contracts;
using TwitterFeed.Repositories.ServiceRepo.ReadFileRepo;
using TwitterFeed.Repositories.ServiceRepo.ReadFileRepo.Contracts;

namespace TwitterFeed
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IReadFileRepo _readFileRepo = new ReadFileRepo();
                IPrintFeed _printFeed = new PrintFeed();
                IConfigService _configService = new ConfigService();

                BuildFeed constructFeed = new BuildFeed(_readFileRepo, _printFeed, _configService);
                constructFeed.Build();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occured: {ex.Message}");
                Console.ReadLine();
            }
        }
    }
}
