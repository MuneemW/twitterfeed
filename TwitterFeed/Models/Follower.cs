﻿using System.Collections.Generic;

namespace TwitterFeed.Models
{
    public class Follower : User
    {
        public List<string> Follows { get; set; }
    }
}
