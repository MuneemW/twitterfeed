﻿using System.Collections.Generic;

namespace TwitterFeed.Models
{
    public class Message : User
    {
        public List<string> Tweet { get; set; }
    }
}
