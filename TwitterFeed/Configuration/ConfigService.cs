﻿using TwitterFeed.Configuration.Contracts;

namespace TwitterFeed.Configuration
{
    public class ConfigService : IConfigService
    {
        public string GetFullFilePath(string filePath, string filename)
        {
            return $"{filePath}/{filename}";
        }
    }
}
