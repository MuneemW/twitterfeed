﻿namespace TwitterFeed.Configuration.Contracts
{
    public interface IConfigService
    {
        string GetFullFilePath(string filePath, string filename);
    }
}
