﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TwitterFeed.Models;
using TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo.Contracts;

namespace TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo
{
    public class PrintFeed : IPrintFeed
    {
        public StringBuilder PrintTweets(List<Follower> followers, List<Message> messages)
        {
            if (followers == null) throw new ArgumentNullException(nameof(followers));
            if (messages == null) throw new ArgumentNullException(nameof(messages));

            StringBuilder constructFeed = new StringBuilder();

            foreach (var follow in followers.OrderBy(x => x.Name))
            {
                constructFeed.AppendLine(follow.Name);

                foreach (var message in messages)
                {
                    if (follow.Follows.Any(x => x == message.Name) || message.Name == follow.Name)
                    {
                        foreach (var tweet in message.Tweet)
                        {
                            constructFeed.AppendLine(String.Format("\t @{0}: {1}", message.Name, tweet));
                        }
                    }
                }
                constructFeed.AppendLine();
            }
            return constructFeed;
        }
    }
}
