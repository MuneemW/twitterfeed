﻿using System;
using System.Collections.Generic;
using System.IO;
using TwitterFeed.Repositories.ServiceRepo.ReadFileRepo.Contracts;

namespace TwitterFeed.Repositories.ServiceRepo.ReadFileRepo
{
    public class ReadFileRepo : IReadFileRepo
    {
        public Dictionary<string, List<string>> ReadTweetFile(string filepath)
        {

            if (string.IsNullOrEmpty(filepath)) throw new ArgumentException($"'{nameof(filepath)}' cannot be null or empty.", nameof(filepath));
            if (!File.Exists(filepath)) throw new FileNotFoundException($"File not found: '{filepath}'");

            string[] tweetFileData = File.ReadAllLines(filepath);

            if (FileHasData(tweetFileData))
            {
                var dictionary = new Dictionary<string, List<string>>();

                foreach (var tweet in tweetFileData)
                {
                    var userAndTweet = tweet.Split('>');

                    if (userAndTweet[1].Length > 140)
                        throw new Exception($"Error Text Can't be longer then 140 charaters. Charater length {userAndTweet[1].Length}");

                    if (!dictionary.ContainsKey(userAndTweet[0]))
                    {
                        var usertweet = new List<string> { userAndTweet[1] };
                        dictionary.Add(userAndTweet[0], usertweet);
                    }
                    else
                    {
                        dictionary[userAndTweet[0]].Add(userAndTweet[1]);
                    }
                }
                return dictionary;
            }
            return new Dictionary<string, List<string>>();

        }

        private bool FileHasData(string[] FileData)
        {
            if (FileData.Length == 0)
                throw new ArgumentException("Error: File cant be empty.");
            else
                return true;
        }

        public Dictionary<string, List<string>> ReadUserFile(string filepath)
        {
            if (string.IsNullOrEmpty(filepath)) throw new ArgumentException($"'{nameof(filepath)}' cannot be null or empty.", nameof(filepath));
            if (!File.Exists(filepath)) throw new FileNotFoundException(filepath);

            string[] userFileData = File.ReadAllLines(filepath);

            if (FileHasData(userFileData))
            {
                var dictionary = new Dictionary<string, List<string>>();

                foreach (var user in userFileData)
                {
                    var userAndFollower = user.Replace(" ", "").Split("follows");
                    var splitUser = userAndFollower[1].Split(',');

                    if (!dictionary.ContainsKey(userAndFollower[0]))
                    {
                        AddUserAndUserTweet(dictionary, splitUser, userAndFollower);
                    }
                    else
                    {
                        AddUserTweet(dictionary, splitUser, userAndFollower);
                    }
                }
                return dictionary;
            }
            return new Dictionary<string, List<string>>();
        }

        private void AddUserTweet(Dictionary<string, List<string>> dictionary, string[] splitUser, string[] userAndFollower)
        {
            if (splitUser.Length > 1)
            {
                foreach (var split in splitUser)
                {
                    if (!dictionary[userAndFollower[0]].Contains(split))
                    {
                        dictionary[userAndFollower[0]].Add(split);
                    }
                }
            }
            else
            {
                if (!dictionary[userAndFollower[0]].Contains(userAndFollower[1]))
                {
                    dictionary[userAndFollower[0]].Add(userAndFollower[1]);

                    if (userAndFollower.Length > 1)
                    {
                        dictionary.Add(userAndFollower[1], new List<string>());
                    }
                }
            }
        }

        private void AddUserAndUserTweet(Dictionary<string, List<string>> dictionary, string[] splitUser, string[] userAndFollower)
        {
            var usertweet = new List<string>();

            if (splitUser.Length > 1)
            {
                foreach (var split in splitUser)
                {
                    usertweet.Add(split);
                }
            }
            else
            {
                usertweet.Add(userAndFollower[1]);
            }

            dictionary.Add(userAndFollower[0], usertweet);

            if (userAndFollower.Length > 1)
            {
                dictionary.Add(userAndFollower[1], new List<string>());
            }
        }
    }
}
