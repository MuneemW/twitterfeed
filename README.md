# README #

### What is this repository for? ###
#### This project was designed for the purpose of:
* Reading a list of users together with their respective followers
* Reading a list of tweets with the follower linked to the tweet
* Displaying the tweets to the console

### How do I get set up? ###

* Ensure that .net core 3.1 is installed on your PC before you run it
* The unit tests can be run in the TwitterFeed.Tests project within the same solution

