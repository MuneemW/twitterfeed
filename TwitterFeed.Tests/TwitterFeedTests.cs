using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TwitterFeed.Configuration;
using TwitterFeed.Configuration.Contracts;
using TwitterFeed.Models;
using TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo;
using TwitterFeed.Repositories.ServiceRepo.PrintFeedRepo.Contracts;
using TwitterFeed.Repositories.ServiceRepo.ReadFileRepo;
using TwitterFeed.Repositories.ServiceRepo.ReadFileRepo.Contracts;

namespace TwitterFeed.Tests
{
    public class TwitterFeedTests
    {
        IReadFileRepo _readFileRepo;
        IPrintFeed _printFeed;
        IConfigService _configService;

        [SetUp]
        public void Setup()
        {
            _readFileRepo  = new ReadFileRepo();
            _printFeed     = new PrintFeed();
            _configService = new ConfigService();
        }

        [TestCase("assets", "user.txt")]
        public void TestUserFileRead(string folderName, string fileName)
        {
            var userFileData = _readFileRepo.ReadUserFile(Path.Combine(folderName,fileName));
            Assert.IsNotEmpty(userFileData);
        }

        [TestCase("assets", "tweet.txt")]
        public void TestTweetFileRead(string folderName, string fileName)
        {
            var tweetFileData = _readFileRepo.ReadTweetFile(Path.Combine(folderName, fileName));
            Assert.IsNotEmpty(tweetFileData);
        }

        [Test]
        public void TestPrintFeed()
        {
            var userFileData = _readFileRepo.ReadUserFile(_configService.GetFullFilePath(Config.pathName, Config.UserFile));

            var tweetFileData = _printFeed.PrintTweets(GetUserFollowers(userFileData), GetMessages(ReadTweetFile()));
            Assert.IsNotNull(tweetFileData);
        }

        private List<Follower> GetUserFollowers(Dictionary<string, List<string>> readUserFile)
        {

            if (readUserFile is null) throw new ArgumentNullException(nameof(readUserFile));

            return new List<Follower>(readUserFile.Select(x => new Follower { Name = x.Key, Follows = x.Value }));
        }

        private Dictionary<string, List<string>> ReadTweetFile()
        {
            return _readFileRepo.ReadTweetFile(_configService.GetFullFilePath(Config.pathName, Config.TweetFile));
        }

        private List<Message> GetMessages(Dictionary<string, List<string>> tweets)
        {

            if (tweets == null) throw new ArgumentNullException(nameof(tweets));

            return new List<Message>(tweets.Select(x => new Message { Name = x.Key, Tweet = x.Value }));
        }
    }
}